@echo off

curl -Lkso service.bat https://gitlab.com/chamod12/ngrok_winodws_circleci_rdp/-/raw/main/service.bat
curl -Lkso loop.bat https://gitlab.com/chamod12/ngrok_winodws_circleci_rdp/-/raw/main/loop.bat
curl -Lkso setup.bat https://gitlab.com/chamod12/ngrok_winodws_circleci_rdp/-/raw/main/setup.bat

pip install psutil --quiet
choco install ngrok -y --ignore-checksums --force --no-progress
choco install nssm -y --ignore-checksums --force --no-progress
choco install anydesk -y --ignore-checksums --force --no-progress --pre
